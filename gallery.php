<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Traditional Greek dancing in London">
    <meta name="keywords" content="LTE.london, Greek, Dance, Dancing, London, classes, lessons, traditional, Greek dance classes, lykion, lykio, lykeio, likeio, likio, ellinidwn, ellinidon, hellinidon, londino, classes">
    <meta http-equiv="Cache-control" content="max-age=604800">

    <title>Lykion Image Gallery</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/grayscale.css" rel="stylesheet">

    <!--Colorbox CSS -->
    <!--<link rel="stylesheet" href="css/colorbox.css">-->

    <link rel="stylesheet" href="css/blueimp-gallery.min.css">

    <!-- Custom Fonts -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/">
                    <span class="light">Lykion Ton Hellinidon</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a href="about.html">About Us</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.html#activities">Activities</a>
                    </li>
                    <li>
                        <a href="gallery.php">Gallery</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.html#social-media">Social Media</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact Us</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro intro-header" style="background-image: url('img/contact-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="brand-heading">
                        <h1 class="brand-heading">Gallery</h1>
                        <h2 class="subheading">Browse photos from our live performances</h2>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-10">
                  <p>
                    <h1>Galleries</h1>
                    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                        <div class="slides"></div>
                        <h3 class="title"></h3>
                        <a class="prev">&lsaquo;</a>
                        <a class="next">&rsaquo;</a>
                        <a class="close">&times;</a>
                        <a class="play-pause"></a>
                        <ol class="indicator"></ol>
                    </div>
                    <!--<ul class="nav nav-tabs nav-stacked">
                        <li role="presentation" class="active"><a href="#">Home</a></li>
                        <li role="presentation"><a href="#">Profile</a></li>
                        <li role="presentation"><a href="#">Messages</a></li>
                    </ul>-->
                    <?php
                        // $dir = 'img/gallery';
                        // $galleries = scandir($dir, 1);
                        
                        // foreach ($galleries as $gallery) {
                        //     if ($gallery == '.' || $gallery == '..' || $gallery == '.DS_Store' || $gallery == 'Thumbs.db') {
                        //         continue;
                        //     } else {
                        //         print("<div id=".str_replace(" ", "_", $gallery).">");
                        //         print("<br/ ><h4>".$gallery."</h4>");
                        //         $gallery_url = $dir."/".$gallery."/large";  
                        //         $image_list = scandir($gallery_url);
                        //         foreach ($image_list as $image) {
                        //             if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                        //                 continue;
                        //             } else {
                        //                 print("<a class='thumbnails' href='".$gallery_url."/".$image."'><img src='".$gallery_url."/thumb/tn-".$image."'></a>");
                        //             }
                        //         }
                        //         print("</div>");                              
                        //     }
                        // }
                    ?>
                    <ul class="nav nav-pills nav-stacked col-md-3">
                        <li class="active"><a href="#Brighton_Greek_Community_2016" data-toggle="pill">Brighton Greek Community 2016</a></li>
                        <li><a href="#Macedonian_Society_of_GB_2016" data-toggle="pill">Macedonian Society of GB 2016</a></li>
                        <li><a href="#Marylebone_Street_Fayre_2016" data-toggle="pill">Marylebone Street Fayre 2016</a></li>
                        <li><a href="#Vasilopita_2016" data-toggle="pill">Vasilopita 2016</a></li>
                        <li><a href="#Marylebone_Street_Fayre_2014" data-toggle="pill">Marylebone Street Fayre 2014</a></li>
                        <li><a href="#Hellenic_Engineers_Society_Annual_Dinner_2013" data-toggle="pill">Hellenic Engineers Society Annual Dinner 2013</a></li>
                        <li><a href="#Marylebone_Street_Fayre_2013" data-toggle="pill">Marylebone Street Fayre 2013</a></li>
                        <li><a href="#Hellenic_Engineers_Society_Annual_Dinner_2010" data-toggle="pill">Hellenic Engineers Society Annual Dinner 2010</a></li>
                        <li><a href="#Hellenic_Engineers_Society_Annual_Dinner_2009" data-toggle="pill">Hellenic Engineers Society Annual Dinner 2009</a></li>
                        <li><a href="#Vasilopita_2009" data-toggle="pill">Vasilopita 2009</a></li>
                        <li><a href="#Blatna_Castle_Czech_Republic_2007" data-toggle="pill">Blatna Castle Czech Republic 2007</a></li>
                        <li><a href="#Hellenic_Engineers_Society_Annual_Dinner_2007" data-toggle="pill">Hellenic Engineers Society Annual Dinner 2007</a></li>
                        <li><a href="#Road_to_Independence_2007" data-toggle="pill">Road to Independence 2007</a></li>
                    </ul>
                    <div class="tab-content col-md-9">
                        <div class="tab-pane active" id="Brighton_Greek_Community_2016">
                            <div id="Brighton_Greek_Community_2016">
                                <h4>Brighton Greek Community 2016</h4>
                                <?php 
                                    $dir = 'img/gallery/Brighton Greek Community 2016/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Macedonian_Society_of_GB_2016">                        
                            <div id="Macedonian_Society_of_GB_2016">
                                <h4>Macedonian Society of GB 2016</h4>
                                <?php 
                                    $dir = 'img/gallery/Macedonian Society of GB 2016/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Marylebone_Street_Fayre_2016">   
                            <div id="Marylebone_Street_Fayre_2016">
                                <h4>Marylebone Street Fayre 2016</h4>
                                <?php 
                                    $dir = 'img/gallery/Marylebone Street Fayre 2016/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Vasilopita_2016">
                            <div id="Vasilopita_2016">
                                <h4>Vasilopita 2016</h4>
                                <?php 
                                    $dir = 'img/gallery/Vasilopita 2016/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Marylebone_Street_Fayre_2014">
                            <div id="Marylebone_Street_Fayre_2014">
                                <h4>Marylebone Street Fayre 2014</h4>
                                <?php 
                                    $dir = 'img/gallery/Marylebone Street Fayre 2014/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Hellenic_Engineers_Society_Annual_Dinner_2013">
                            <div id="Hellenic_Engineers_Society_Annual_Dinner_2013">
                                <h4>Hellenic Engineers Society Annual Dinner 2013</h4>
                                <?php 
                                    $dir = 'img/gallery/Hellenic Engineers Society Annual Dinner 2013/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Marylebone_Street_Fayre_2013">
                            <div id="Marylebone_Street_Fayre_2013">
                                <h4>Marylebone Street Fayre 2013</h4>
                                <?php 
                                    $dir = 'img/gallery/Marylebone Street Fayre 2013/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Hellenic_Engineers_Society_Annual_Dinner_2010">
                            <div id="Hellenic_Engineers_Society_Annual_Dinner_2010">
                                <h4>Hellenic Engineers Society Annual Dinner 2010</h4>
                                <?php 
                                    $dir = 'img/gallery/Hellenic Engineers Society Annual Dinner 2010/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Hellenic_Engineers_Society_Annual_Dinner_2009">
                            <div id="Hellenic_Engineers_Society_Annual_Dinner_2009">
                                <h4>Hellenic Engineers Society Annual Dinner 2009</h4>
                                <?php 
                                    $dir = 'img/gallery/Hellenic Engineers Society Annual Dinner 2009/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Vasilopita_2009">
                            <div id="Vasilopita_2009">
                                <h4>Vasilopita 2009</h4>
                                <?php 
                                    $dir = 'img/gallery/Vasilopita 2009/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Blatna_Castle_Czech_Republic_2007">
                            <div id="Blatna_Castle_Czech_Republic_2007">
                                <h4>Blatna Castle Czech Republic 2007</h4>
                                <?php 
                                    $dir = 'img/gallery/Blatna Castle Czech Republic 2007/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Hellenic_Engineers_Society_Annual_Dinner_2007">
                            <div id="Hellenic_Engineers_Society_Annual_Dinner_2007">
                                <h4>Hellenic Engineers Society Annual Dinner 2007</h4>
                                <?php 
                                    $dir = 'img/gallery/Hellenic Engineers Society Annual Dinner 2007/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Road_to_Independence_2007">
                            <div id="Road_to_Independence_2007">
                                <h4>Road to Independence 2007</h4>
                                <?php 
                                    $dir = 'img/gallery/Road to Independence 2007/large';
                                    $image_list = scandir($dir);
                                    foreach ($image_list as $image) {
                                        if ($image == '.' || $image == '..' || $image == '.DS_Store' || $image == 'Thumbs.db' || $image == 'thumb') {
                                            continue;
                                        } else {
                                            print("<a class='thumbnails' href='".$dir."/".$image."'><img src='".$dir."/thumb/tn-".$image."'></a>");
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                  </div>
                    </div>
                  </p>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                  </br>
                </div>
            </div>
        </div>
    </article>

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; Lykion Ton Hellinidon 2016</p>             
            <p>Built by <a href="https://barisi.me" target="_blank">Barisi</a> &amp; Hosted by <a href="http://www.zarifis.net" target="_blank">Zarifis Consulting Ltd.</a></p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>

    <script src="js/blueimp-gallery.min.js"></script>
    <script>
        <?php
            $dir = 'img/gallery';
            $galleries = scandir($dir, 1);

            foreach ($galleries as $gallery) {
                if ($gallery == '.' || $gallery == '..' || $gallery == '.DS_Store' || $gallery == 'Thumbs.db') {
                    continue;
                } else {
                    $links = str_replace(" ", "_", $gallery);
                    print("
                    document.getElementById('".$links."').onclick = function (event) {
                        event = event || window.event;
                        var target = event.target || event.srcElement,
                            link = target.src ? target.parentNode : target,
                            options = {index: link, event: event},
                            ".$links." = this.getElementsByTagName('a');
                        blueimp.Gallery(".$links.", options);
                    };\n");
                }
            }
        ?>
    </script>
    
    <!-- Google analytics code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
        Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-96916120-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>